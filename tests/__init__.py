# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from .test_stock_shipment_out_go_and_print import suite

__all__ = ['suite']
